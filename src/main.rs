use sqlx::sqlite::SqlitePoolOptions;
use std::time::SystemTime;
use sqlx::Connection;

#[derive(Debug)]
struct Message {
    sender: String,
    receiver: String,
    timestamp: u64,
    message: String,
    room: String,
}

#[derive(Debug)]
struct DisplayMsg {
    sender: String,
    receiver: String,
    message: String,
}

fn get_unix_timestamp() -> u64{
    SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_secs()
}

/*
fn add_msg(conn: dyn Connection, sender: String, receiver: String, message: String, room: String) -> Result<()>{

    match conn.execute("INSERT INTO messages (sender, receiver, timestamp, message, room) VALUES (?1, ?2, ?3, ?4, ?5)",
    params![sender, receiver, get_unix_timestamp(), message, room]){
        Ok(updated) => println!("{} rows were updated", updated),
        Err(err) => println!("update failed: {}", err),
    }
}*/
#[async_std::main]
async fn main() -> Result<(), sqlx::Error> {
    let path = "./db_v0.db3";

    let conn = SqlitePoolOptions::new().connect("sqlite::memory").await?;

    conn.execute(
        "CREATE TABLE messages (
                  id              INTEGER PRIMARY KEY,
                  sender            TEXT NOT NULL,
                  receiver            TEXT NOT NULL,
                  timestamp            INTEGER,
                  message   TEXT NOT NULL,
                  room  TEXT NOT NULL
                  )",
        [],
    )?;
    

    let mut stmt = conn.prepare("SELECT sender, receiver, message FROM messages")?;
    let messages_iter = stmt.query_map([], |row| {
        Ok(DisplayMsg {
            sender: row.get(0)?,
            receiver: row.get(1)?,
            message:  row.get(2)?,
        })
    })?;

    for msg in messages_iter {
        println!("Found person {:?}", msg.unwrap());
    }
    Ok(())
}